require 'rcov/rcovtask'

desc 'Measures test coverage using rcov'
namespace :rcov do
  desc 'Output unit test coverage.'
  Rcov::RcovTask.new(:unit) do |rcov|
    rcov.pattern    = '**/*Tests.rb'
    rcov.output_dir = 'rcov'
    rcov.verbose    = true
  end
end