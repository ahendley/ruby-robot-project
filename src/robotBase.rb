require './core'

class RobotBase
  include Compass
  
  attr_reader :x, :y, :facing, :table
  attr_writer :facing

  def place(x = 0, y = 0, facing = Compass::NORTH)
    if (x < 0) || (x > @table.width)
      raise "Value for 'x' is outside of acceptable range."
    end
    if (y < 0) || (y > @table.height)
      raise "Value for 'y' is outside of acceptable range."
    end
    if (facing < Compass::NORTH) || (facing > Compass::WEST)
      raise "Value for 'facing' is outside of acceptable range."
    end
    
    @x = x
    @y = y
    @facing = facing
  end

  def turnLeft
    checkStatus
    
    @facing -= 1

    if (@facing < Compass::NORTH)
      @facing = Compass::WEST
    end
  end

  def turnRight
    checkStatus
    
    @facing += 1

    if (@facing > Compass::WEST)
      @facing = Compass::NORTH
    end
  end

  def moveForward
    checkStatus 
        
    tx = @x
    ty = @y

    case @facing
    when Compass::NORTH
      ty += 1
    when Compass::SOUTH
      ty -= 1
    when Compass::EAST
      tx += 1
    when Compass::WEST
      tx -= 1
    end

    if (tx < @x) && (tx < 0)
      raise "Any further move WEST will cause death."
    elsif (tx > @x) && (tx >= @table.width)
      raise "Any further move EAST will cause death."
    elsif (ty < @y) && (ty < 0)
      raise "Any further move SOUTH will cause death."
    elsif (ty > @y) && (ty >= @table.height)
      raise "Any further move NORTH will cause death."
    else
      @x = tx
      @y = ty
    end
  end
  
  def status
    return @x, @y, @facing
  end

  def checkStatus   
    if (@x == nil) || (@y == nil) || (@x == -1) || (@y == -1)
      puts "Sorry, no can do. You need to place me somewhere on the table first."
    elsif (@facing == nil) || (@facing == -1)
      puts "Sorry, I do not know which way I am facing. Please point me in the right direction."
    end
  end

end