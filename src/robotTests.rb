require 'test/unit'
require './core.rb'
require './robot.rb'

class TestRobot < Test::Unit::TestCase
  
  def setup
    puts ""
    puts "Performing test setup"

    file_1 = "directions_1.txt"
    file_2 = "directions_2.txt"
    file_3 = "directions_3.txt"
    file_4 = "directions_4.txt"
    file_5 = "directions_5.txt"
    
    @directions_1 = Array.new
    @directions_2 = Array.new
    @directions_3 = Array.new
    @directions_4 = Array.new
    @directions_5 = Array.new
    
    IO.foreach(file_1) { |line| @directions_1.push(line.chomp) }
    IO.foreach(file_2) { |line| @directions_2.push(line.chomp) }
    IO.foreach(file_3) { |line| @directions_3.push(line.chomp) }
    IO.foreach(file_4) { |line| @directions_4.push(line.chomp) }
    IO.foreach(file_5) { |line| @directions_5.push(line.chomp) }
  end

  def teardown
    # code here if needed.
  end
  
  def test_directions_1_length
    puts "Performing directions 1 length test"
    assert_equal(@directions_1.length, 3, "The number of directions for set 1 is not what was expected")   
  end
  
  def test_directions_2_length
    puts "Performing directions 2 length test"
    assert_equal(@directions_2.length, 3, "The number of directions for set 2 is not what was expected")   
  end
  
  def test_directions_3_length
    puts "Performing directions 3 length test"
    assert_equal(@directions_3.length, 6, "The number of directions for set 3 is not what was expected")   
  end

  def test_robot_initial_state
    puts "Performing robot initial status test"
    
    myrobot = Robot.new(Table.new(5,5))
    
    x, y, facing = myrobot.status
    assert(x == -1, "The robot status for value 'x' is not equal to -1")
    assert(y == -1, "The robot status for value 'y' is not equal to -1")
    assert(facing == -1, "The robot status for value 'facing' is not equal to -1")
  end
  
  def test_robot_1_navigation
    puts "Performing robot 1 navigation test"
    
    myrobot = Robot.new(Table.new(5,5))
    moveRobot(myrobot, @directions_1)

    x, y, facing = myrobot.status
    assert((x == 0) && (y == 1) && (facing == Compass::NORTH), "The robot's expected location has not been reached")
  end
  
  def test_robot_2_navigation
    puts "Performing robot 2 navigation test"
    
    myrobot = Robot.new(Table.new(5,5))
    moveRobot(myrobot, @directions_2)

    x, y, facing = myrobot.status
    assert((x == 0) && (y == 0) && (facing == Compass::WEST), "The robot's expected location has not been reached")
  end
  
  def test_robot_3_navigation
    puts "Performing robot 3 navigation test"
    
    myrobot = Robot.new(Table.new(5,5))
    moveRobot(myrobot, @directions_3)

    x, y, facing = myrobot.status
    assert((x == 3) && (y == 3) && (facing == Compass::NORTH), "The robot's expected location has not been reached")
  end
  
  def test_robot_4_navigation
    puts "Performing robot 4 navigation test"
    
    myrobot = Robot.new(Table.new(5,5))
    moveRobot(myrobot, @directions_4)

    x, y, facing = myrobot.status
    assert((x == 0) && (y == 1) && (facing == Compass::WEST), "The robot's expected location has not been reached")
  end
  
  def test_robot_5_navigation
    puts "Performing robot 5 navigation test"
    
    myrobot = Robot.new(Table.new(5,5))
    moveRobot(myrobot, @directions_5)

    x, y, facing = myrobot.status
    assert((x == 0) && (y == 1) && (facing == Compass::EAST), "The robot's expected location has not been reached")
  end
  
  def moveRobot(robot, directions)
    for cmd in directions
      parts = cmd.split(" ")
      
      puts "Executing command - " + parts[0].upcase
      
      case parts[0].upcase
      when "PLACE"
        args = parts[1].split(",")
        robot.place(Integer(args[0]), Integer(args[1]), robot.getDirectionFromString(args[2]))
      when "MOVE"
        robot.moveForward
      when "LEFT"
        robot.turnLeft
      when "RIGHT"
        robot.turnRight
      when "REPORT"
        robot.reportStatus        
      end
    end
  end
end