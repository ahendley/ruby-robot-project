
module Compass
  NORTH = 0
  EAST = 1
  SOUTH = 2
  WEST = 3
  
  def getDirectionName(direction)
    if (direction == NORTH)
      return "North"
    elsif (direction == EAST)
      return "East"
    elsif (direction == SOUTH)
      return "South"
    elsif (direction == WEST)
      return "West"
    else
      return "Unknown"
    end
  end
  
  def getDirectionFromString(stringValue)
    case stringValue.upcase
    when "NORTH"
      return NORTH
    when "EAST"
      return EAST
    when "SOUTH"
      return SOUTH
    when "WEST"
      return WEST
    end
  end
end