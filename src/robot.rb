require './core'

class Robot < RobotBase

  def initialize(table)
    @table = table
    @x = -1
    @y = -1
    @facing = -1
  end
  
  def moveForward
    begin
      super
    rescue => ex
      puts "Sorry, I cannot move any further. %s" % [ex.message]      
    end    
  end

  def reportStatus
    tx, ty, tfacing = self.status
    face_name = getDirectionName(tfacing)
    
    if ( face_name == "Unknown" )
      face_name = " - Oh no. I appear to be lost."  
    end
    
    puts "At position " + String(tx) +  "," + String(ty) + " facing "+ face_name
  end
end