
class Table
  attr_reader :width, :height
  
  def initialize(width = 5, height = 5)
    @width = width
    @height = height
  end
end